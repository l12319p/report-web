require("../../common/manifest.js");
require("../../common/vendor.js");
global.webpackJsonp([5],{

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(70);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_c4a18be6_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(73);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(71)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_c4a18be6_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/password/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c4a18be6", Component.options)
  } else {
    hotAPI.reload("data-v-c4a18be6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 71:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_s__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_s___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__utils_s__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_index__ = __webpack_require__(2);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      account: {
        'mobile': '',
        'password': '',
        'rePassword': ''
      }
    };
  },

  methods: {
    clickSave: function clickSave() {
      console.log(this.account);
      var $this = this;
      wx.request({
        url: __WEBPACK_IMPORTED_MODULE_0__utils_s___default.a.prefixHttp + 'report-service/api/member/register',
        method: 'POST',
        data: $this.account,
        success: function success(res) {
          console.log(res);
          Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["d" /* showToast */])('注册成功');
        },
        fail: function fail(res) {}
      });
    }
  }
});

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "info"
  }, [_c('div', {
    staticClass: "zk-group"
  }, [_c('div', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model.lazy",
      value: (_vm.account.mobile),
      expression: "account.mobile",
      modifiers: {
        "lazy": true
      }
    }],
    staticClass: "zk-group_input",
    attrs: {
      "type": "text",
      "placeholder": "手机号",
      "eventid": '0'
    },
    domProps: {
      "value": (_vm.account.mobile)
    },
    on: {
      "change": function($event) {
        _vm.account.mobile = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('p', [_vm._v("您输入的手机号不正确")])], 1), _vm._v(" "), _c('div', {
    staticClass: "zk-group"
  }, [_c('div', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model.lazy",
      value: (_vm.account.password),
      expression: "account.password",
      modifiers: {
        "lazy": true
      }
    }],
    staticClass: "zk-group_input",
    attrs: {
      "type": "text",
      "placeholder": "密码",
      "eventid": '1'
    },
    domProps: {
      "value": (_vm.account.password)
    },
    on: {
      "change": function($event) {
        _vm.account.password = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('p', [_vm._v("您输入的手机号不正确")])], 1), _vm._v(" "), _c('div', {
    staticClass: "zk-group"
  }, [_c('div', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model.lazy",
      value: (_vm.account.rePassword),
      expression: "account.rePassword",
      modifiers: {
        "lazy": true
      }
    }],
    staticClass: "zk-group_input",
    attrs: {
      "type": "text",
      "placeholder": "确认密码",
      "eventid": '2'
    },
    domProps: {
      "value": (_vm.account.rePassword)
    },
    on: {
      "change": function($event) {
        _vm.account.rePassword = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('p', [_vm._v("您输入的手机号不正确")])], 1), _vm._v(" "), _c('div', {
    staticClass: "zk-group"
  }, [_c('button', {
    staticClass: "zk-group-btn",
    attrs: {
      "eventid": '3'
    },
    on: {
      "click": function($event) {
        _vm.clickSave()
      }
    }
  }, [_vm._v("保存")])], 1)])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-c4a18be6", esExports)
  }
}

/***/ })

},[69]);
//# sourceMappingURL=main.js.map