require("../../common/manifest.js");
require("../../common/vendor.js");
global.webpackJsonp([1],{

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(61);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_92186dc4_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(68);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(62)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_92186dc4_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/my/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-92186dc4", Component.options)
  } else {
    hotAPI.reload("data-v-92186dc4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 62:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_s__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_s___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__utils_s__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      selectClassAValue: 1,
      isActive: false,
      avatarUrl: '',
      nickName: '',
      canIUse: wx.canIUse('button.open-type.getUserInfo'),
      tabA: [{ id: 1, title: '我的收藏', img: __webpack_require__(64) }, { id: 2, title: '我的下载', img: __webpack_require__(65) }, { id: 3, title: '我的订阅', img: __webpack_require__(66) },
      // {id: 4, title: '邀请好友', img: require('../../../static/images/4.png')},
      { id: 4, title: '修改密码', img: __webpack_require__(67) }]
    };
  },

  methods: {
    selectCalssA: function selectCalssA(item) {
      this.selectClassAValue = item;
      if (item === 4) {
        console.log('路由跳转');
        wx.navigateTo({
          url: '/pages/password/main'
        });
      } else if (item === 1) {
        wx.navigateTo({
          url: '/pages/collection/main'
        });
      } else if (item === 2) {
        wx.navigateTo({
          url: '/pages/download/main'
        });
      } else if (item === 3) {
        wx.navigateTo({
          url: '/pages/login/main'
        });
      }
    },
    systemInfo: function systemInfo() {
      var url = '/pages/info/main';
      wx.navigateTo({ url: url });
    },
    bindGetUserInfo: function bindGetUserInfo(e) {
      var userInfo = e.target.userInfo;
      var $this = this;
      wx.login({
        success: function success(res) {
          console.log(res, '测试');
          if (res.code) {
            // 获取微信登录的code缓存 在接口调用的时候使用
            // wx.setStorage({
            //   key: 'code',
            //   data: res.code
            // })
            wx.request({
              url: __WEBPACK_IMPORTED_MODULE_0__utils_s___default.a.prefixHttp + 'report-service/api/member/wxLogin',
              data: {
                code: res.code
              },
              success: function success(res) {
                console.log(res);
                $this.isActive = true;
              },
              fail: function fail(res) {
                console.log(res);
                $this.isActive = false;
              }
            });
          } else {
            console.log('登录失败！' + res.errMsg);
          }
        }
      });
      this.avatarUrl = userInfo.avatarUrl;
      this.nickName = userInfo.nickName;
    },
    getUserInfo: function getUserInfo() {
      console.log('click事件首先触发', this.canIUse, this.isActive);
      // 判断小程序的API，回调，参数，组件等是否在当前版本可用。  为false 提醒用户升级微信版本
      // console.log(wx.canIUse('button.open-type.getUserInfo'))
      if (wx.canIUse('button.open-type.getUserInfo')) {
        // 用户版本可用
      } else {
        console.log('请升级微信版本');
      }
    },
    onLoad: function onLoad() {
      var $this = this;
      wx.getSetting({
        success: function success(res) {
          if (res.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: function success(res) {
                var userInfo = res.userInfo;
                $this.avatarUrl = userInfo.avatarUrl;
                $this.nickName = userInfo.nickName;
                $this.isActive = true;
                console.log(res.userInfo);
              }
            });
          }
        }
      });
    }
  },
  created: function created() {
    this.onLoad();
  }
});

/***/ }),

/***/ 64:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA6CAYAAADspTpvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkI0Njk4MDgzQTUwNjExRTg5Rjc3RTdGOEQ2RURCNzk0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkI0Njk4MDg0QTUwNjExRTg5Rjc3RTdGOEQ2RURCNzk0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjQ2OTgwODFBNTA2MTFFODlGNzdFN0Y4RDZFREI3OTQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjQ2OTgwODJBNTA2MTFFODlGNzdFN0Y4RDZFREI3OTQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4Bi2qxAAAFmElEQVR42tyba2wUVRTHz93W2u62RUuxNFKk3doiolgSRDC2CCqPShNr8ItRSzUhQVT40MQPJho1Bnx+AjUmVkkMQqKA8RFBE9qGBhVQiIUS+zKprVQg1L6wlh3/hz2Lm6Xb7s7cuw9O8k+bmc655zf3Meeemaqy7y6SYUuBZkGFUA50HZQp5wah89A5qBPqgowGlGrAZzq0BCqHKqD5ciwSuwAdhRqgRuiAHNNmSlMPK2gZ9Cj0EDRFU3z90G7oE+h7yHLq0OXw+mugJ6Bj0H6oRiMsia8a8X1c2kqLF/Bq6AT0EXQbmbe50lYLVBVL4BnQ19AXUDHF3rjNvRLDDNPA1TJ8V1L8baXEUm0CmB8tb0OfyaMlUSxHYnpHYtQCnAF9Dm2ixLWNEmOGU2A3tMfJIhFDq5K57bYLzENkB/QAJY/dLzGn2AF+PUl6dryefiNa4DUJPmcjmdNrIgUugD6UdDFZTQlDQSTA7wbtZoyYGzMsK9U4dKawTAj8MFRpKoIpyLzful1RY4WLDkBbyxTlpBmFrhSmcYH5nm8x2frLcxTdO02RS/nH3KIcRa/eanzmbAneBgcD89bOa6rVBdcruif3Sri7AL14qlFor7BdAfycyRXkmeLwUBu8yvQK+Wwo8DyozFRrSzCM52aHPz87CxlDnlHk+cJ4GbjWVEs8X5/2Tg6zvkhRitlurg0GXmWqlQenKyryTP53M5EBr843SlwZAC40sZFPRezFeBKuK4ocYl2hohJck+YyAsyLVyEv10udeOFhWIBNmTdTkdfDPyGPutRjqVF2WF460acLXeSziP64QNQxZFH7IP8k6sTvXcNEI85qjstS7S5W6ciW6koULcdi407RP+/5JhZkKKrI/X+t5xuxr8+i11otGhyz5bqMgWfbufL5UkVV+bFNt/lGrMjzg7/QYqtiW8qz5SY7V953Q/z2Fkun2W57JgNPtXPlwL/x2wr1j9m+NJuBbRXO3++04gb8XofttrNcZLMYv6fHos2nrEvzKVZ2EW29ctKivT22G3Uz7Ijdq3d1W7TpuOX0URGRDaONjccs2t3j6A4PM/B5Jx6azlj01BEfnR01B8u+uY2DZx0Pp34G7nXq5eQA0WM/+ah9SD8sJx2Pw3frgBZ3vQzcpsPTn8iMag/7qO8fvT3LPnv1vSFuY+Bfdc6zbI21Kq57DeldH1oY+Edt2bnHn3LqMt5EeD1agX9g4GZISxoxJ1t/9nWLPp/M2MzAvBw06fA4UVXDNnCWNlfMOBBIOnYmWG+YGDU7gyseu5wkIIH5drOB8j371FD6GRHGy8CcfGx34jHSDT8/V186YdGLUEcEz+1rEeGNGY6BtwcSrOCHyGZoLdn8SuYvPH/HrPDQv6C5j3+3qBGZWSBf+rLXovJcRTWzFM0Ls4UZ9ZHTLG5U2IhCNg5dUL3tnA1r4AchOyjeWDQAcC2Sh1qkhg1BsGz8e+D8kzjfFHKebVu7RUNjjoDrhc1fNwn5MG06dIr3jXa98xuG8lwshwhyf59FnVGmm1zh5KpGJsZe4xmiQ+cc5c9/S0WnNxww23poK10dtiGUJdzr0m+vAlhm2BZ6cDxgHkP8iV93EsN2C4MVCTDbafJ/JDqchLDDEvvp8U5OVN45LBeOJhEsx1otsVO0wGz7BHokCWBHBHbC9SeSAh5/xLmc9+MJDMtf1K+AvprsDyOtWPJOYyF0JAFh+Qv6O8n/BT3pAmZrhxZDb3ISlQCgPollkcRGuoEDi0IddHece/uoxFAX7aJq903sIRlG/Fa9LYag7dLmAokhanM5HFL1kqs+IqUiU9YsbZRKm7anlNL8f0scEH/juEp6wW4Nc0yepd/Ixr1VV4DK4D9qeWSe3QGVcPECypfj7qCsaEh2M7+JfoYOynHt9p8AAwDe4Gz83W25BQAAAABJRU5ErkJggg=="

/***/ }),

/***/ 65:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA7CAYAAAAn+enKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjE4QTJBMEE4QTUwNzExRTg5Rjc3RTdGOEQ2RURCNzk0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjE4QTJBMEE5QTUwNzExRTg5Rjc3RTdGOEQ2RURCNzk0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MThBMkEwQTZBNTA3MTFFODlGNzdFN0Y4RDZFREI3OTQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MThBMkEwQTdBNTA3MTFFODlGNzdFN0Y4RDZFREI3OTQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5udd1XAAAEbklEQVR42uSbXUgUURTHj7vrdxil1GqhWUEffiaFaVAPGYRQURilIAb50ksvlVBQUFD5EoYPvRUoiWVoEoUJWtkHLUSaZR9Cmm5mgq0ptIWE2Tl6dh2W1Z3dvXfcWQ/8LdeZe8/vzj0z99w5GzJZcww0NgNqMf9/DPVPy85NEttORO1AbUaloJJQ8ahol+PsqO+oftQH1CtUG8qqB+BsVCFqH2qVynNoANaydio+70PdQ9WiLKIcDBEwpaNQR1HH2WkZ9hlVibrBM8KvePLVwlFneCpWSoQFbruSrzr1Gak1ME3ZLtRFVJyG95w47vMt+yAdOAZVjWqUfEXVXPFG9iVGFvAmVAeqGALHitmnLNHAe1HPUKsh8Ix8eso+CgEuQjW4eX4GkkWzj0X+Au/nODFC4JuRfT3gKzCtkm7pBFYJXcu+ewW8AnUbFQb6M/K5jhlUARv5hOWgX1vGDCY1a+kyVK7Q7ldm4Mp6y9zH9GHOMNApstdcZrk0F3Ay6pzw8Y4xY+7k4VE58hV/dIru+SzH9JfZpnQ5KgKCxyKYyW0Mp6MOQvAZMWW4Az5J6WIQAhPTCVfgJagCCF4rYEYn8GF/ckwdWCQzOoEPQfCbE5gW3jkLAHgrapGB151hCwCYGLcTcDYsHMumlVaqsObCMToyMaM0rwMIVdwDjaGez03LB9i4a+b3v38AhroB3twFGLeL8jDNJHQXI6cEIMHH8aNBUQ5MWBTAGlwOR8YAPLkmysNkmtLxwoDN68VPQrFtxirf8/hvw73igYd7hKYxBqHJgqUawD4izj27DeBllXDgCXEOImxLhRhogqW2fo+KBJ4gYKEtOh2lf/1uY0R0gIwRsE143E05fNU3aHmwU9sMBuVuwLxfabmwZL0E/EnausYR079sgQBL1k3A7VIXcwTQ6gHaGQJSYck6CPi59BWsE/qH2Hj33p4SMK0WerSBRrDRbzOfjQ5qCUuMvY5tWnoRdUoT6KbLAEsT+Z5pBZjUrIinQbnjcVOzBI0AbX3TmtS0YqlGCUwlBBYIXiO2TiUw2ZUgBnaymVzm+DtKklU1sSEPIGX3/Lj//iHAxxa1R3c54tcVmAKKXj41qWomfY+6nQwZlprvDXAZKMobXd8t4dBBvapm1HcoYb30WO2R9a4X0F0lnpmnQazO45Ye7rTfNKT80N0LcTqghB4gOoYl34+4ws4GTPYAdV7HwBdQ9939weDhpBodwtbMdbEMHqYFTe06HcHe8RSOnuq0aL+Lir2u6wCWfCwED3t0airxqIFS1GkQueEnzibYt1I1/nlTXEq1ElSxbg0gWCv7VK72BG/Lh+m7CJmoqnl+bFHf1exLmzcn+lIg/pOfcbnzlGFZuO8S9gVkA7t2TBlEqwagj7gvvwbaIGBqNaPyYLrsqQI1IBBygNtM51ht9jeUQiR8UYvKhLLYwW2cbiapGFzKaPo5RX3Bs6Zd9L3CJOmG8prlMPoGDL2HToDpmhJHsbmdNQjTm4njsuPivwADANM8J+8aDyHEAAAAAElFTkSuQmCC"

/***/ }),

/***/ 66:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA6CAYAAADspTpvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkI0Njk4MDg3QTUwNjExRTg5Rjc3RTdGOEQ2RURCNzk0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkI0Njk4MDg4QTUwNjExRTg5Rjc3RTdGOEQ2RURCNzk0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjQ2OTgwODVBNTA2MTFFODlGNzdFN0Y4RDZFREI3OTQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjQ2OTgwODZBNTA2MTFFODlGNzdFN0Y4RDZFREI3OTQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7uv4y0AAAGIElEQVR42tRaaUxUVxQ+bxYEWrawSBAaqAKCVFArCNRWrCCNjemSNk2MpkljWpr+sP1XbX90SUxXk/4itTWmNU3a1LRpIyoK2kq1C2hFQKi27CBgkUWpDG9mes7MGUGUGd68e2c5yfcDZubc89177tneUz7YfRMkihGxhJGJSEJEIKL58xHEOKIP0Ya4zLDKMsgkQWcOYhPiYcRaJqhFaAPqED8jqhCNgUg4BrEd8TwiS6cu2qDHGLsRFxH7EZ8hhvUaatD5+2TEHkQn4j0BZO8mWay7g9dK8QfhMMQuRCtihxdu6+3J7+C7vott8AnhhxAtiHcR94DvJYzXbuEYIY0w3fe3EScRqeB/IRtOIN7REosMGoISRcw3OdUEipAtb7BtMaIIJ3GKKIXAlVK2MUkv4XjEMc6tgS45bGu8t4SjEEcR2RA8ks02R2klrCC+QKyA4JMVbLuihfDriM0QvEK275wv4QJOP1JEwX2PT1AgNFQ66bcQazzV0mauWaWknrT7DfBIiQni4hWYmLDDvk8tcFNes0YcPkfkIabmOuFXZETkhIUKPPOcGZ5+1uwgSxIerkDWMukpPZs53fWEI+bye28lMlKBorVGWJZjdLjybImNU3xxn3ey147PJvwSIk7ECgvwfq7ON8GD+UYwuSn6Jid9EsCI04uID2cSpq2u0N1r4gVZnmeEYjzVsDDPp9fZYfNV1H4Z8RHC7iJcQjFFj8b0DGdAio6Zn5sODtihu9NnhNOYY62L8DY92srKTY6T1SJXh2xgt/s0NxPHWgO780Y9mjKXao+22RjIklMMviRcTlxpxQcQiXo0NdR7N2QsRc8w+q7ZXEhcifA6vZpO16lQfUR1FBNaJDZWgdUFPm2v19EdzhWhqfFPKzRfsMKiZIOjuKDgtXKVZzJrikzQetEGI9fsEBLiLD0lpqtcIpwuSpsVPbsLI29XJ49JkDSVk27nRmjBlm1mMJuVWzl7EsvNwUEbXGqzQXOTVeQGZJA1ybK2s+aYCqrq+XuUs2cWKFS4pNxngPWlJthesQDSM4UFtyTSFCuLMLnpb2dUXTqoq9r8hBkyxJCOIC2RMqPE779aYXhYX8Kle71ho5CHJJHSEyHd66ZGqxBvESAG2rbriHulVe4YsQuLTbo27FyDFc6cVkWYM0aWXJNFmFyxfBNFYO9+39Zqg1M/qaJOl2ScCPeAzgdUc5acWQZITNTe8/b12uBEjRX6+4Q3F1eI8N+IQhmEl+dqq6LGRu1IVIVLf0nroi5T0DovQzP1xkmLtMVE6p462qW2jOfJolMyNIeGKm6nHSRUlHTN6ImjohUoKDTJJFxHhOs5cAkVi8U+Z79L/6dURVPLg99MwfC/01+kZiImRsqsi94n+YMIU5KsEa2dTo+mGrOFXPbL/RY4UqXC2JjdkXaoBHUJtYuPlkk55ePE1XXJDshYYWaffHXI7jjNb7+eumMjaLZFHZNLUtMMkLFUeE3k4OjaykOIAW6ShUkLdjqjI862j07W3UjnZK0KixeHgDnE+XfJehO0/2OBKYsQUwaY461BPPnUXhmn3NtjQ8M9z6+uj9vhl7pp146IxAqtSJhr72WOtz15+ARxA/woZ/EKkOu7hObaNBXRKTeYG8wmPIT42J+EbXiNj1ert+XyvFW6R0B7mNsdhEneR3T5k3RPt80xKnKlL52z625wvuM1PWGZfZXA+cilyp+kjx5WHVF7HO/1TBf3QiqYE8x1wiSHEZX+dm0KdDrJVroisyfCJK9RVQLBK/XMAeZL+D/EU9w6BpuQzU8yh3kTdv2QHsEMBhFZsrXc3UF5qt/ofUZ66tYbBGT72NZmt23rfCpEcL5QeiGAyTYhitlW0EvY0eSwwgMBSJZsKmIbQRRhEnpHYitiC2I0AIiOsi1b2TYQTdglX4HzLfV9lDL9kaZ57Sy2RdvoyctF+xEvUH2P+M5HxGmN7xH5vHa/N0r0dtnnOF9nc2UzKsl1K3kNyq8NepSJajjbuG59FVGGeByxAbx/Uaadx04/IqoRwt7XEz08IsN+YJDQqxQr+XRSEQngfMoRzp9PcHFPBUMn59CziCuy7sX/AgwAdRy2rmSXST4AAAAASUVORK5CYII="

/***/ }),

/***/ 67:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA6CAYAAADspTpvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjE4QTJBMEE0QTUwNzExRTg5Rjc3RTdGOEQ2RURCNzk0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjE4QTJBMEE1QTUwNzExRTg5Rjc3RTdGOEQ2RURCNzk0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjQ2OTgwODlBNTA2MTFFODlGNzdFN0Y4RDZFREI3OTQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjQ2OTgwOEFBNTA2MTFFODlGNzdFN0Y4RDZFREI3OTQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz461FSzAAAFoUlEQVR42tRbXUwdRRQ+s720BW6hPymCoG1AwAqk0dRCfNBCtSa2kpRa06StL60mRmNqjDXxxWqiT33QEKsxJvrmT5q2UR6kTfxLCpWmf5Riy09qE2n1CgmUAgLCeM7dc2GX7r3c3Z1Z7j3Jl8DOnTnn2505c87ZWTH1wfugSQTiQUQVogzxAOJeRDaDZIRxE9GD6EJcRlxFSB1GhRSPl4t4FlGPqEOs8jjOAOJHxHeI7xFDqUZ4A+IAogGRqWA8ulE7GWOIY4iPEGf9Dmz47L+JnwQZslsR2bmSyWO3sa5NC0F4Dd/1nxC1EJzUsk7SvTYIwuSIXkb8jtgOCyeku5NtEboIr0ScQBzRNHW9TPUjbNNK1YTLeQ3VQ+pJPdtWrooweeDTiBJIXSlhGzf4Jfww4pSP/TRIWcW2PuKVMEVGzYjlkD5Ctv7AtrsiHGbXvxrST1az7WE3hD/hGDhdhWz/NNnQ8jnEHj0TbjmIvHvw3nPucGcEZORvgMFBHdp285Z1NBHhHESj2pxJgKiqAvHoRoC8PMdIBiIRkGfbQF7GREkqTZIa2ZENxZvSbyPylalbtgyMPXtBbN3mSHZGsI1+Y+x9IdpHoeQzJ8c1TBa9qkxVOBwlC0VFyfcpLDT7hMMqSb/C3O6a0m9YEnP/M/mZrdE1axV5/TpAezuu24j5G3rqNN2Li+3rHPvKb79RZUo2c3vLSpji0pfUTaR8ECWWwGx6GmRTE8grHfYb0P8PpgBXACoqcUrjDVq0yLwR2FcWFADcuqXKIuJ2iHJrw5J9KAswRJk9rJWtLXeRtbVjm2xttY9RWqY6INluXcO7lHrmnJw5hDrn7SI7OxOOoUB2xQgvRTypdGhjjvOfnJi/z9zfGIZqwsRxKY1anRL57dgYwISF9PBtHflzTYgJL7xMToI8ikHRRgxQhgZBtrTo0FJNhNenSgAsb/wBQNAn60NKE/vMLIBcdDaZ9hVC+63MyvI25hBO7bFRVRYWE+FC38MsWQJiy9MgKiudt6mdz7urtDltW82Ymo+P+7W0iJzWCt/77lNb4pJVsq9TYII3VIHkGirCSVFern19izIlgUhYzWa3eLF+j6ZGx78hzhVzA/PEvb2mJ8Y8GdastScO+mWYCA8EQnhqCuTxYyC7u2evnTkDUFoKomGHjsjKSW6Tlj8DebLnz9nJxq7jNWoLSPqI8LVAVPX0xm/r7gmKcBcRvhiIKkN4a1MrF4lwSyCqHqrw1qZWThPhdkRE+z5KpZyaGtM7z1wUIKqro20BCHFsJ8LTYJ6j0B881NaBWLdu9n/8W9RtDurpNhHX2F7wZWA7YYYlgMjICHIP/iJW8YjObTCPDOl/ylWVZtSECGgqA3MjjjNVSyr309v0D7Wrvu9+MA68bv7NVcoA5GPmaCvEf4b4y9NwExPufk9E3ZJ1q2NWiNPnMzugpYHOQx3yFEVd0x+7yC7PK444jToRBr4T510bc+pkwrqzb7JUADjZ7KXrBevTjfoQh7OWVOOiQyLu87HsbNUvwzC/GQYYGfG00MAsUNoiSaf3w5cQBz05MDLMm3E65KBT2BwvJ6NzjV9B+srXzAGSJUyyH/FbGpIlm/fFzVMSdCTPto2neLrIJbZ51Athkn7E5jR50m1sa3/CLDWJgagERC+ijqcw2RNMdmDetDzJAe8gdiDeRIynENFxtqmBbQRVhGPx9mHEY4iOFCDbwbYcBhffR3gpFVIkRmcw6QDM4AIQJZ2vsQ2uo0KvtdH/OAOhM43vzOcoFEk/x8Wks5FtgKAIWx3ae2B+EvAi4meuoKgSGusXHpt0vJuMY0okqr5qGeUgnUBvI+lTnscRT4D5rZIbuckkf+WyjNK6eUjD1OsD82Bn7HAnEa7gJ1SAoOwi9qaDXvNgdgB0PukGmN8x9OlcF/8LMAA3vk4y4YQ0AQAAAABJRU5ErkJggg=="

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "my"
  }, [_c('div', {
    staticClass: "my-top"
  }, [_c('div', {
    staticClass: "my-person"
  }, [_c('div', {
    staticClass: "my-null"
  }), _vm._v(" "), _c('div', {
    staticClass: "my-image"
  }, [_c('div', [_c('img', {
    attrs: {
      "src": _vm.avatarUrl,
      "alt": ""
    }
  })]), _vm._v(" "), _c('p', [_vm._v(_vm._s(_vm.nickName))])], 1), _vm._v(" "), _c('div', {
    staticClass: "my-system",
    attrs: {
      "eventid": '0'
    },
    on: {
      "click": function($event) {
        _vm.systemInfo()
      }
    }
  }, [_c('img', {
    attrs: {
      "src": "../../../static/images/system.png",
      "alt": ""
    }
  }), _vm._v(" 设置")])]), _vm._v(" "), _c('div', {
    staticClass: "my-profit"
  }, [_c('div', [_c('div', [_vm._v("12345")]), _vm._v(" "), _c('p', [_vm._v("可提收益")])], 1), _vm._v(" "), _c('div', [_c('div', [_vm._v("12345")]), _vm._v(" "), _c('p', [_vm._v("消费之处")])], 1), _vm._v(" "), _c('div', [_c('div', [_vm._v("12345")]), _vm._v(" "), _c('p', [_vm._v("已提收益")])], 1)])]), _vm._v(" "), _c('div', {
    staticClass: "my-bottom"
  }, _vm._l((_vm.tabA), function(item, index) {
    return _c('div', {
      key: item.title,
      class: {
        classAactive: _vm.selectClassAValue === item.id
      }
    }, [_c('div', {
      attrs: {
        "eventid": '1-' + index
      },
      on: {
        "click": function($event) {
          _vm.selectCalssA(item.id)
        }
      }
    }, [_c('div', {
      staticClass: "iconA"
    }, [_c('img', {
      attrs: {
        "src": item.img,
        "alt": ""
      }
    }), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.title))])], 1), _vm._v(" "), _vm._m(0, true)]), _vm._v(" "), (_vm.canIUse && !_vm.isActive) ? _c('button', {
      staticClass: "button-getuserInfo",
      attrs: {
        "open-type": "getUserInfo",
        "eventid": '2-' + index
      },
      on: {
        "getuserinfo": _vm.bindGetUserInfo,
        "click": _vm.getUserInfo
      }
    }) : _vm._e()], 1)
  }))])
}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "iconB"
  }, [_c('img', {
    attrs: {
      "src": "../../../static/images/arrow.png",
      "alt": ""
    }
  })])
}]
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-92186dc4", esExports)
  }
}

/***/ })

},[60]);
//# sourceMappingURL=main.js.map