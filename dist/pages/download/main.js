require("../../common/manifest.js");
require("../../common/vendor.js");
global.webpackJsonp([11],{

/***/ 30:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(31);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_5dcd83ba_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(34);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(32)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_5dcd83ba_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/download/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5dcd83ba", Component.options)
  } else {
    hotAPI.reload("data-v-5dcd83ba", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 32:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_card__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_index__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__utils_storage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_s__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_s___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__utils_s__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



// import { get } from '@/utils/http'


/* harmony default export */ __webpack_exports__["a"] = ({
  onload: function onload() {
    wx.showShareMenu({
      withShareTicket: true
    });
    // 调用应用实例的方法获取全局数据
    this.getReportList(1);
    this.getReportIndustryList();
  },
  data: function data() {
    return {
      total: 0,
      dataList: [],
      userInfo: {},
      page: 1,
      isActive: true
    };
  },


  components: {
    card: __WEBPACK_IMPORTED_MODULE_0__components_card__["a" /* default */]
  },
  methods: {
    bindViewTap: function bindViewTap(item) {
      var url = '/pages/detail/main';
      wx.navigateTo({ url: url });
      console.log(url);
    },
    scroll: function scroll() {},

    // 滚动加载数据
    scrolltolower: function scrolltolower() {
      this.page = this.page + 1;
      console.log('数据可视化');
      this.getReportList(this.page);
    },
    scrollLeftList: function scrollLeftList(item) {
      console.log(this.selectClassB.length);
    },

    // 获取报告列表
    getReportList: function getReportList(i) {
      var $this = this;
      var page = i;
      // 数据加载动画
      Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["b" /* showLoad */])('加载中');
      wx.request({
        url: __WEBPACK_IMPORTED_MODULE_3__utils_s___default.a.prefixHttp + ('report-service/api/report/download/list?page=' + page + '&pageSize=' + 10),
        method: 'GET',
        header: { 'token': __WEBPACK_IMPORTED_MODULE_2__utils_storage___default.a.get('token') },
        success: function success(res) {
          wx.hideLoading();
          var data = res.data.data;
          $this.isActive = false;
          if (!data.download) {
            Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["b" /* showLoad */])('没有更多数据了');
            return;
          }
          for (var _i = 0; _i < data.download.length; _i++) {
            data.download[_i].createTime = Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["a" /* formatTime */])(new Date(data.download[_i].createTime));
            $this.dataList.push(data.download[_i]);
          }
          $this.total = data.total;
        },
        fail: function fail(res) {}
      });
    },
    clickHandle: function clickHandle(msg, ev) {
      console.log('clickHandle:', msg, ev);
    }
  },
  onShareAppMessage: function onShareAppMessage(res) {
    return {
      title: '自定义转发标题',
      path: '/page/user?id=123'
    };
  },
  created: function created() {
    // 调用应用实例的方法获取全局数据
    this.getReportList(1);
  }
});

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroll-view', {
    style: ({
      'height': '100vh'
    }),
    attrs: {
      "scroll-y": true,
      "eventid": '1'
    },
    on: {
      "scrolltolower": _vm.scrolltolower,
      "scroll": _vm.scroll
    }
  }, [_c('div', {
    staticClass: "data-list"
  }, [_c('ul', _vm._l((_vm.dataList), function(item, index) {
    return _c('li', {
      key: item,
      attrs: {
        "eventid": '0-' + index
      },
      on: {
        "click": function($event) {
          _vm.bindViewTap(item)
        }
      }
    }, [_c('div', {
      staticClass: "logo"
    }, [_c('img', {
      attrs: {
        "src": 'http://112.74.47.88:8090/report-res/' + item.cover,
        "alt": ""
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "data-list-title"
    }, [_c('div', [_vm._v(_vm._s(item.name))]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.createTime) + "\n              ")])], 1), _vm._v(" "), _c('div', {
      staticClass: "data-list-right"
    }, [_c('div', [_c('img', {
      attrs: {
        "src": "../../../static/images/see.png",
        "alt": ""
      }
    }), _vm._v(" "), _c('div', [_vm._v(_vm._s(item.looktimes))])])])])
  }))], 1), _vm._v(" "), (_vm.isActive) ? _c('div', {
    staticClass: "collection-none"
  }, [_vm._v("您还没有收藏")]) : _vm._e()])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-5dcd83ba", esExports)
  }
}

/***/ })

},[30]);
//# sourceMappingURL=main.js.map