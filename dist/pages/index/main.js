require("../../common/manifest.js");
require("../../common/vendor.js");
global.webpackJsonp([10],{

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(36);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_0d9f62b6_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(39);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(37)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0d9f62b6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_0d9f62b6_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/index/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0d9f62b6", Component.options)
  } else {
    hotAPI.reload("data-v-0d9f62b6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 37:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 38:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_card__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_index__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__utils_storage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_s__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_s___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__utils_s__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



// import { get } from '@/utils/http'


/* harmony default export */ __webpack_exports__["a"] = ({
  onload: function onload() {
    wx.showShareMenu({
      withShareTicket: true
    });
  },
  data: function data() {
    return {
      motto: 'Hello World',
      selectTop: ['最新报告', '订阅报告'],
      selectClassB: [],
      selectClassAValue: '最新报告',
      selectClassBValue: 1,
      total: 0,
      dataList: [],
      userInfo: {},
      page: 1,
      collectId: 0
    };
  },


  components: {
    card: __WEBPACK_IMPORTED_MODULE_0__components_card__["a" /* default */]
  },
  methods: {
    bindViewTap: function bindViewTap(item) {
      var url = '/pages/detail/main?id=' + item.id;
      wx.navigateTo({ url: url });
    },

    // 最新报告按钮tab
    selectCalssA: function selectCalssA(item) {
      this.selectClassAValue = item;
    },

    // 二级筛选条件
    selectCalssB: function selectCalssB(item) {
      this.selectClassBValue = item.id;
      this.dataList = [];
      this.page = 0;
      this.getReportList(item.name);
    },

    // 点击显示收藏信息
    showModalTab: function showModalTab(e) {
      wx.showActionSheet({
        itemList: ['发送邮箱'],
        success: function success(res) {
          if (res.tapIndex === 0) {
            var url = '/pages/info/main';
            wx.navigateTo({ url: url });
          }
        },
        fail: function fail(res) {},
        complete: function complete(res) {}
      });
    },
    scroll: function scroll() {},

    // 滚动加载数据
    scrolltolower: function scrolltolower() {
      this.getReportList();
    },
    scrollLeftList: function scrollLeftList(item) {},

    // 获取报告列表
    getReportList: function getReportList(industry) {
      this.page = this.page + 1;
      var $this = this;
      // 数据加载动画
      Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["b" /* showLoad */])('加载中');
      wx.request({
        url: __WEBPACK_IMPORTED_MODULE_3__utils_s___default.a.prefixHttp + ('report-service/api/report/list?page=' + this.page + '&pageSize=' + 10 + '&industry=' + industry),
        method: 'GET',
        success: function success(res) {
          wx.hideLoading();
          var data = res.data.data;
          if (!data.reports.length) {
            Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["c" /* showLoadTime */])('没有更多数据了');
            return;
          }
          for (var i = 0; i < data.reports.length; i++) {
            data.reports[i].createTime = Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["a" /* formatTime */])(new Date(data.reports[i].createTime));
            $this.dataList.push(data.reports[i]);
          }
          $this.total = data.total;
        },
        fail: function fail(res) {}
      });
    },

    // 获取行业列表
    getReportIndustryList: function getReportIndustryList() {
      var $this = this;
      console.log('哈哈哈了', __WEBPACK_IMPORTED_MODULE_2__utils_storage___default.a.get('token'));
      wx.request({
        url: __WEBPACK_IMPORTED_MODULE_3__utils_s___default.a.prefixHttp + 'report-service/api/industry/list',
        method: 'GET',
        success: function success(res) {
          var data = res.data.data;
          $this.selectClassB = data.industries;
          $this.selectClassBValue = $this.selectClassB[0].id;
          $this.getReportList($this.selectClassB[0].name);
        },
        fail: function fail(res) {}
      });
    },
    clickHandle: function clickHandle(msg, ev) {
      console.log('clickHandle:', msg, ev);
    },

    // 点击收藏
    clickCollect: function clickCollect(item, status) {
      var $this = this;
      if (status === 2) {
        wx.request({
          url: __WEBPACK_IMPORTED_MODULE_3__utils_s___default.a.prefixHttp + ('report-service/api/collection/collect/' + item.id),
          method: 'GET',
          header: { 'token': __WEBPACK_IMPORTED_MODULE_2__utils_storage___default.a.get('token') },
          success: function success(res) {
            $this.dataList.forEach(function (e) {
              if (e.id === item.id) {
                e.isCollection = 1;
              }
            });
            Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["d" /* showToast */])('收藏成功');
          },
          fail: function fail(res) {}
        });
      } else if (status === 1) {
        wx.request({
          url: __WEBPACK_IMPORTED_MODULE_3__utils_s___default.a.prefixHttp + ('report-service/api/collection/cancel/' + item.id),
          method: 'GET',
          header: { 'token': __WEBPACK_IMPORTED_MODULE_2__utils_storage___default.a.get('token') },
          success: function success(res) {
            $this.dataList.forEach(function (e) {
              if (e.id === item.id) {
                e.isCollection = 0;
              }
            });
            Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["d" /* showToast */])('取消收藏');
          },
          fail: function fail(res) {}
        });
      }
    }
  },
  onShareAppMessage: function onShareAppMessage(res) {
    return {
      title: '自定义转发标题',
      path: '/page/user?id=123'
    };
  },
  created: function created() {
    // 调用应用实例的方法获取全局数据
    this.getReportIndustryList();
  }
});

/***/ }),

/***/ 39:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroll-view', {
    style: ({
      'height': '100vh'
    }),
    attrs: {
      "scroll-y": true,
      "eventid": '9'
    },
    on: {
      "scrolltolower": _vm.scrolltolower,
      "scroll": _vm.scroll
    }
  }, [_c('div', {
    staticClass: "index",
    attrs: {
      "eventid": '8'
    },
    on: {
      "click": function($event) {
        _vm.clickHandle('test click', $event)
      }
    }
  }, [_c('div', {
    staticClass: "search"
  }, [_c('input', {
    attrs: {
      "type": "text",
      "placeholder": "数据名称/描述/标签"
    }
  }), _vm._v(" "), _c('p', [_vm._v("搜索")])], 1), _vm._v(" "), _c('div', {
    staticClass: "index-total"
  }, [_vm._v("已收录报告"), _c('span', [_vm._v(_vm._s(_vm.total))]), _vm._v("份")]), _vm._v(" "), _c('div', {
    staticClass: "index-select-top"
  }, _vm._l((_vm.selectTop), function(item, index) {
    return _c('p', {
      key: item,
      class: {
        classAactive: _vm.selectClassAValue === item
      },
      attrs: {
        "eventid": '0-' + index
      },
      on: {
        "click": function($event) {
          _vm.selectCalssA(item)
        }
      }
    }, [_vm._v(_vm._s(item))])
  })), _vm._v(" "), _c('div', {
    staticClass: "index-value-classB"
  }, [_c('scroll-view', {
    attrs: {
      "scroll-x": true,
      "eventid": '2'
    },
    on: {
      "scroll": _vm.lower
    }
  }, [_c('div', {
    staticClass: "tab"
  }, _vm._l((_vm.selectClassB), function(item, index) {
    return _c('p', {
      key: item.tille,
      class: {
        classBactive: _vm.selectClassBValue === item.id
      },
      attrs: {
        "eventid": '1-' + index
      },
      on: {
        "click": function($event) {
          _vm.selectCalssB(item)
        }
      }
    }, [_vm._v(_vm._s(item.name))])
  }))]), _vm._v(" "), _c('img', {
    attrs: {
      "src": "../../../static/images/select.png",
      "alt": "",
      "eventid": '3'
    },
    on: {
      "click": function($event) {
        _vm.scrollLeftList(_vm.item)
      }
    }
  })], 1), _vm._v(" "), _c('div', {
    staticClass: "data-list"
  }, [_c('ul', _vm._l((_vm.dataList), function(item, index) {
    return _c('li', {
      key: item,
      attrs: {
        "data": "1",
        "eventid": '7-' + index
      },
      on: {
        "click": function($event) {
          _vm.bindViewTap(item)
        }
      }
    }, [_c('div', {
      staticClass: "logo"
    }, [_c('img', {
      attrs: {
        "src": 'http://112.74.47.88:8090/report-res/' + item.cover,
        "alt": ""
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "data-list-title"
    }, [_c('div', [_vm._v(_vm._s(item.name))]), _vm._v(" "), _c('p', [_vm._v(_vm._s(item.createTime) + "\n              "), (item.isCollection) ? _c('img', {
      attrs: {
        "src": '../../../static/images/collect.png',
        "alt": "",
        "eventid": '4-' + index
      },
      on: {
        "click": function($event) {
          $event.stopPropagation();
          _vm.clickCollect(item, 1)
        }
      }
    }) : _vm._e(), _vm._v(" "), (!item.isCollection) ? _c('img', {
      attrs: {
        "src": '../../../static/images/cancel.png',
        "alt": "",
        "eventid": '5-' + index
      },
      on: {
        "click": function($event) {
          $event.stopPropagation();
          _vm.clickCollect(item, 2)
        }
      }
    }) : _vm._e()])], 1), _vm._v(" "), _c('div', {
      staticClass: "data-list-right"
    }, [_c('div', [_c('img', {
      attrs: {
        "src": "../../../static/images/see.png",
        "alt": ""
      }
    }), _vm._v(" "), _c('div', [_vm._v(_vm._s(item.looktimes))])]), _vm._v(" "), _c('p', [_c('img', {
      attrs: {
        "src": "../../../static/images/ellipsis.png",
        "alt": "",
        "eventid": '6-' + index
      },
      on: {
        "click": function($event) {
          $event.stopPropagation();
          _vm.showModalTab($event)
        }
      }
    })])], 1)])
  }))], 1)])])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0d9f62b6", esExports)
  }
}

/***/ })

},[35]);
//# sourceMappingURL=main.js.map