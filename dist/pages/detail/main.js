require("../../common/manifest.js");
require("../../common/vendor.js");
global.webpackJsonp([12],{

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(26);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_00150603_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(29);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(27)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_00150603_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/detail/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-00150603", Component.options)
  } else {
    hotAPI.reload("data-v-00150603", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 27:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 28:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_s__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_s___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__utils_s__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_index__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_storage__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__utils_storage__);
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      id: ''
    };
  },
  onLoad: function onLoad(option) {
    console.log('haha', option);
    this.downloadReport(option.id);
  },
  onShow: function onShow() {
    // const id = this.$root.$mp.query.id
    // this.id = this.$root.$mp.query.id
  },

  methods: {
    // 获取报告详情
    reportDetail: function reportDetail(id) {
      wx.request({
        url: __WEBPACK_IMPORTED_MODULE_0__utils_s___default.a.prefixHttp + ('report-service/api/report/detail/' + id),
        method: 'GET',
        header: { 'token': __WEBPACK_IMPORTED_MODULE_2__utils_storage___default.a.get('token') },
        success: function success(res) {
          var data = res.data.data;
          console.log(data);
        },
        fail: function fail(res) {}
      });
    },

    // 点击下载报告
    downloadReport: function downloadReport(id) {
      var $this = this;
      Object(__WEBPACK_IMPORTED_MODULE_1__utils_index__["b" /* showLoad */])('加载中');
      wx.request({
        url: __WEBPACK_IMPORTED_MODULE_0__utils_s___default.a.prefixHttp + ('report-service/api/report/download/' + id),
        method: 'GET',
        header: { 'token': __WEBPACK_IMPORTED_MODULE_2__utils_storage___default.a.get('token') },
        success: function success(res) {
          var data = res.data.data;
          var path = data.path;
          $this.createPdf(path);
        },
        fail: function fail(res) {
          return false;
        }
      });
    },

    // 生成pdf
    createPdf: function createPdf(tempFilePath) {
      wx.downloadFile({
        url: tempFilePath,
        header: { 'token': __WEBPACK_IMPORTED_MODULE_2__utils_storage___default.a.get('token') },
        success: function success(res) {
          console.log(res);
          var tempFilePath = res.tempFilePath;
          if (res.statusCode === 200) {
            wx.openDocument({
              filePath: tempFilePath,
              success: function success(res) {
                wx.hideLoading();
                console.log('打开文档成功');
              }
            });
          }
        }
      });
    }
  }
});

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div')
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-00150603", esExports)
  }
}

/***/ })

},[25]);
//# sourceMappingURL=main.js.map