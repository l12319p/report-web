// let sendRrquest = function (url, method, data) {
//   const urlHost = 'http://112.74.47.88:8090/'
//   let promise = new Promise(function (resolve, reject) {
//     wx.request({
//       url: urlHost + url,
//       data: data,
//       method: method,
//       header: {'token': 'oOO4Y44joqCrmiztuK29BA39uH3w'},
//       success: resolve,
//       fail: reject
//     })
//   })
//   return promise
// }
// module.exports.sendRrquest = sendRrquest
export function get (apiurl, params) {
  wx.showLoading({
    title: '加载中',
    mask: true
  })
  wx.request({
    url: 'http://112.74.47.88:8090/' + apiurl,
    method: 'GET',
    params: params,
    header: {'token': 'oOO4Y44joqCrmiztuK29BA39uH3w'},
    success: function (res) {
      wx.hideLoading()
      console.log('数据那', res.data)
    }
  })
}
export function post (apiurl, params) {
  wx.showLoading({
    title: '加载中',
    mask: true
  })
  wx.request({
    url: 'http://112.74.47.88:8090/' + apiurl,
    method: 'POST',
    data: params,
    header: {'token': 'oOO4Y44joqCrmiztuK29BA39uH3w'},
    success: function (res) {
      wx.hideLoading()
      console.log('数据那', res.data)
    }
  })
}
