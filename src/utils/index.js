function formatNumber (n) {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}
// 加载动画
export function showLoad (title) {
  wx.showLoading({
    title: title,
    mask: true
  })
}
// 加载动画（定时器）
export function showLoadTime (title) {
  wx.showLoading({
    title: title,
    mask: true
  })
  setTimeout(function () {
    wx.hideLoading()
  }, 1000)
}
// 提示动画
export function showToast (title) {
  wx.showToast({
    title: title,
    icon: 'loading',
    image: '../../static/images/success.png',
    duration: 1000,
    mask: false,
    success: function () { },
    fail: function () { }
  })
}
export function getStorage () {
  wx.getStorage({
    key: 'code',
    success: function (res) {
      console.log('缓存数据', res.data)
    }
  })
}
export function formatTime (date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  // const hour = date.getHours()
  // const minute = date.getMinutes()
  // const second = date.getSeconds()

  const t1 = [year, month, day].map(formatNumber).join('-')
  // const t2 = [hour, minute, second].map(formatNumber).join(':')

  return `${t1}`
}

export default {
  formatNumber,
  formatTime,
  showLoad,
  getStorage,
  showToast
}
